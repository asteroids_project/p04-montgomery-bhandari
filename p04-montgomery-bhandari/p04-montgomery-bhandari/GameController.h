//
//  GameController.h
//  p04-montgomery-bhandari
//
//  Created by Montgomery Chelsea on 2/20/16.
//  Copyright © 2016 SUNY_Binghamton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameController : UIViewController

//Timers for the overall game, and to control turning right and left
@property(nonatomic, strong) NSTimer *left_timer;
@property(nonatomic, strong) NSTimer *right_timer;
@property(nonatomic, strong) NSTimer *game_timer;

//Labels for time, score, and lives left
@property (nonatomic, strong) IBOutlet UILabel *lives_label;
@property (nonatomic, strong) IBOutlet UILabel *time_label;
@property (nonatomic, strong) IBOutlet UILabel *score_label;

//Buttons for turning and firing
@property (nonatomic, strong) IBOutlet UIButton *fire_button;
@property (nonatomic, strong) IBOutlet UIButton *turn_left_button;
@property (nonatomic, strong) IBOutlet UIButton *turn_right_button;

//Countdown label
@property (nonatomic, strong) IBOutlet UILabel *countdown_label;

//Image of rocket
@property (nonatomic, strong) IBOutlet UIImageView *rocket_image;

@end

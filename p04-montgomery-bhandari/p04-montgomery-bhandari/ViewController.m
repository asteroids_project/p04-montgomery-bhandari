//
//  ViewController.m
//  p04-montgomery-bhandari
//
//  Created by Montgomery Chelsea on 2/20/16.
//  Copyright © 2016 SUNY_Binghamton. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end



@implementation ViewController


//Synthesize buttons and labels
@synthesize start_button;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

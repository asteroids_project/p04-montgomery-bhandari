//
//  ViewController.h
//  p04-montgomery-bhandari
//
//  Created by Montgomery Chelsea on 2/20/16.
//  Copyright © 2016 SUNY_Binghamton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

//Start button
@property (nonatomic, strong) IBOutlet UIButton *start_button;


@end


//
//  GameController.m
//  p04-montgomery-bhandari
//
//  Created by Montgomery Chelsea on 2/20/16.
//  Copyright © 2016 SUNY_Binghamton. All rights reserved.
//

#import "GameController.h"

@interface GameController ()

@end

@implementation GameController

//debug variable
int debug = 1;

//Synthesize labels
@synthesize lives_label;
@synthesize score_label;
@synthesize time_label;
@synthesize countdown_label;

//Sythesize buttons
@synthesize turn_left_button;
@synthesize turn_right_button;
@synthesize fire_button;

//Synthesize rocket
@synthesize rocket_image;

//Game timers
@synthesize game_timer;
@synthesize left_timer;
@synthesize right_timer;

//Current level of the game - updated every thirty seconds
int level = 1;
//Current seconds the game has been running
int seconds;

//Current angle of rocket ship
float angle = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (debug)
    {
        NSLog(@"Loaded game screen");
    }
    
    //Disable buttons at first
    fire_button.enabled = NO;
    turn_right_button.enabled = NO;
    turn_left_button.enabled = NO;
    
    //Reset all labels and countdown
    [self performSelector:@selector(reset_and_countdown) withObject:self afterDelay:1.0f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reset_two_seconds
{
    [countdown_label setText:@"2..."];
    [self performSelector:@selector(reset_one_second) withObject:self afterDelay:1.0f];
}

-(void)reset_one_second
{
    [countdown_label setText:@"1..."];
    [self performSelector:@selector(reset_go) withObject:self afterDelay:1.0f];
}

-(void)reset_go
{
    [countdown_label setText:@"GO!"];
    [self performSelector:@selector(reset_hide) withObject:self afterDelay:1.5f];
}

//Hides reset label, enables buttons, and begins game timer function
-(void)reset_hide
{
    //Reset timer label and seconds
    seconds = 0;
    NSString *cur_time_string = [NSString stringWithFormat:@"00:%02d", seconds];
    [time_label setText:cur_time_string];
    
    
    //Hide the countdown label and enable the buttons
    [countdown_label setHidden:YES];
    
    fire_button.enabled = YES;
    turn_right_button.enabled = YES;
    turn_left_button.enabled = YES;
    
    //Begin game timer
    [self game_begin];
    
}

//Game begin function
-(void)game_begin
{
    
    if(debug)
    {
        NSLog(@"Starting game!");
    }
    
    game_timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                  target:self
                                                selector:@selector(updateTimer)
                                                userInfo:nil repeats:YES];
}
                                                                   
//Update timer
-(void)updateTimer
{
    ++seconds;
    int seconds_elapsed = seconds % 60;
    int minutes_elapsed = seconds / 60;
    NSString *cur_time_string = [NSString stringWithFormat:@"%02d:%02d", minutes_elapsed, seconds_elapsed];
    [time_label setText:cur_time_string];
    
        
}
                                                        

//Resets score, lives, and timers, then counts down
-(void)reset_and_countdown
{
    if(debug)
    {
        NSLog(@"reseting and counting down");
    }
    
    //Disable buttons - they are reenabled after the countdown
    fire_button.enabled = NO;
    turn_right_button.enabled = NO;
    turn_left_button.enabled = NO;
    
    //Countdown on label from 3 .. 2 .. 1 .. GO!
    [countdown_label setHidden:NO];
    [countdown_label setText:@"3..."];
    [self performSelector:@selector(reset_two_seconds) withObject:self afterDelay:1.0f];
    
}

//Rotate image to the left
-(void)turn_left
{
    angle -= 0.01f;
    rocket_image.transform = CGAffineTransformMakeRotation(angle);
}

//Rotate image to the right
-(void)turn_right
{
    angle += 0.01f;
    rocket_image.transform = CGAffineTransformMakeRotation(angle);
}


//Fuction for turning left
-(IBAction)start_turn_left:(id)sender
{
    if(debug)
    {
        NSLog(@"Turning left");
    }

    left_timer = [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(turn_left) userInfo:nil repeats:YES];
}

//Function to stop turning left
-(IBAction)stop_turning_left:(id)sender
{
    if(debug)
    {
        NSLog(@"Stopped turning left");
    }
    
    [left_timer invalidate];
}

//Function for turning right
-(IBAction)start_turn_right:(id)sender
{
    if(debug)
    {
        NSLog(@"Turning right");
    }
    
    right_timer = [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(turn_right) userInfo:nil repeats:YES];
}

//Function to stop turning right
-(IBAction)stop_turning_right:(id)sender
{
    if(debug)
    {
        NSLog(@"Stopped turning right");
    }
    
    [right_timer invalidate];
}

//Function for firing
-(IBAction)fire:(id)sender
{
    if(debug)
    {
        NSLog(@"Firing!");
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
